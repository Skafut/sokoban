package ru.androiddevschool.std;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

import java.util.HashMap;

import ru.androiddevschool.std.Screens.StdScreen;
import ru.androiddevschool.std.Utils.StdAssets;
import ru.androiddevschool.std.Utils.Global;
import ru.androiddevschool.std.Utils.Names;
import ru.androiddevschool.std.Utils.Values;

abstract public class StdGame extends Game implements Names {
    protected StdAssets assets;
    protected HashMap<ScreenName, StdScreen> screens;
    protected AssetManager manager;
    public GameState gameState;

    @Override
    public void create() {
        Global.batch = new SpriteBatch();
        Global.renderer = new ShapeRenderer();
        screens = new HashMap<ScreenName, StdScreen>();
        manager = new AssetManager();
        assets = new StdAssets(manager);
        initValues();
    }

    public StdScreen screen(ScreenName name) {
        return (screens == null ? null : screens.get(name));
    }

    public void saveResult() {}

    public void setScreen(ScreenName name) { if (screens != null && screens.containsKey(name)) setScreen(screens.get(name)); }

    public void initValues(){
        Values.PPU_X = Values.WORLD_WIDTH/ Gdx.graphics.getWidth();
        Values.PPU_Y = Values.WORLD_HEIGHT/ Gdx.graphics.getHeight();
    }

    abstract public void postLoad();
}
