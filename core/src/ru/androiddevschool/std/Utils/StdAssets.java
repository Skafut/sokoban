package ru.androiddevschool.std.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;

/**
 * Created by Гриша on 04.05.2017.
 */
public class StdAssets {
    protected AssetManager manager;
    public HashMap<String, TextureRegionDrawable> images;
    public HashMap<String, Button.ButtonStyle> buttonStyles;

    public StdAssets(AssetManager manager) {
        this.manager = manager;
        initImages();
        initStyles();
    }
    private void initImages() {
        images = new HashMap<String, TextureRegionDrawable>();
        addImagesFolder("/images/bg");
        addImagesFolder("/images/ui");
        addImagesFolder("/images/jelly");
        for (String name : images.keySet())
            System.out.println(name);

    }

    public void addImagesFolder(String folderName) {
        FileHandle file = Gdx.files.local(folderName);
        for (FileHandle f : file.list()) {
            if (f.name().endsWith("png") || f.name().endsWith("jpg"))
                images.put(f.nameWithoutExtension(), getDrawable(f));
        }
    }

    public TextureRegionDrawable getDrawable(FileHandle file) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(file)));
    }

    private void initStyles() {
        buttonStyles = new HashMap<String, Button.ButtonStyle>();
        buttonStyles.put("Button-Right",
                new TextButton.TextButtonStyle(
                        images.get("Button-Left-Up"),
                        images.get("Button-Left-Pressed"),
                        null,
                        new BitmapFont()
                )
        );
        buttonStyles.put("Button-Left",
                new Button.ButtonStyle(
                        images.get("Button-Right-Up"),
                        images.get("Button-Right-Pressed"),
                        null
                )
        );
        buttonStyles.put("Button-Up",
                new Button.ButtonStyle(
                        images.get("Button-Up-Up"),
                        images.get("Button-Up-Pressed"),
                        null
                )
        );
        buttonStyles.put("Button-Down",
                new Button.ButtonStyle(
                        images.get("Button-Down-Up"),
                        images.get("Button-Down-Pressed"),
                        null
                )
        );
        buttonStyles.put("Button-Back",
                new Button.ButtonStyle(
                        images.get("Button-Back-Up"),
                        images.get("Button-Back-Pressed"),
                        null
                )
        );
        buttonStyles.put("Button-Music",
                new Button.ButtonStyle(
                        images.get("Button-Music-Up"),
                        images.get("Button-Music-Pressed"),
                        null
                )
        );
        buttonStyles.put("Long_Button",
                new Button.ButtonStyle(
                        images.get("long-button-down"),
                        images.get("long-button-pressed"),
                        null
                )
        );
        buttonStyles.put("play",
                new Button.ButtonStyle(
                        images.get("play-up"),
                        images.get("play-down"),
                        null
                )
        );
        buttonStyles.put("back",
                new Button.ButtonStyle(
                        images.get("back-up"),
                        images.get("back-down"),
                        null
                )
        );
        buttonStyles.put("help",
                new Button.ButtonStyle(
                        images.get("help-up"),
                        images.get("help-down"),
                        null
                )
        );
        buttonStyles.put("music",
                new Button.ButtonStyle(
                        images.get("music-down"),
                        images.get("music-down"),
                        images.get("music-up")
                )
        );
        buttonStyles.put("sounds",
                new Button.ButtonStyle(
                        images.get("sounds-down"),
                        images.get("sounds-down"),
                        images.get("sounds-up")
                )
        );
        buttonStyles.put("settings",
                new Button.ButtonStyle(
                        images.get("settings-up"),
                        images.get("settings-down"),
                        null
                )
        );
        buttonStyles.put("pause",
                new Button.ButtonStyle(
                        images.get("pause-up"),
                        images.get("pause-down"),
                        null
                )
        );
        buttonStyles.put("touchpad",
                new Button.ButtonStyle(
                        images.get("touchpad-button-up"),
                        images.get("touchpad-button-down"),
                        null
                )
        );
        buttonStyles.put("settings",
                new Button.ButtonStyle(
                        images.get("settings-up"),
                        images.get("settings-down"),
                        null
                )
        );
    }

    private TextureRegionDrawable drawable(Texture texture){
        return new TextureRegionDrawable(new TextureRegion(texture));
    }

}
