package ru.androiddevschool.std.Controller;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import java.util.HashMap;
import java.util.HashSet;

import ru.androiddevschool.std.Utils.Names;

import static ru.androiddevschool.std.Utils.Global.game;

/**
 * Created by Гриша on 04.05.2017.
 */
public class ScreenTraveler extends ClickListener implements Names {
    private ScreenName from;
    private ScreenName to;
    private HashMap<StageType, HashSet<Actor>> transfer;

    public ScreenTraveler(ScreenName from, ScreenName to, HashMap<StageType, HashSet<Actor>> transfer) {
        this.from = from;
        this.to = to;
        this.transfer = transfer;
    }

    public ScreenTraveler(ScreenName name) {
        this(null, name, null);
    }

    public void clicked(InputEvent event, float x, float y) {
        prepare();
    }

    public void prepare() {
        if (from != null) game.screen(from).leavingScreen(this);
        else
            travel();
    }

    public void travel() {
        if (transfer != null) {
            for (StageType type : transfer.keySet())
                game.screen(to).transferActors(transfer.get(type), type);
        }
        game.setScreen(to);
    }
}