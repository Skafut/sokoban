package ru.androiddevschool.sokoban.Controller;

import com.badlogic.gdx.graphics.Camera;

import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;

import ru.androiddevschool.sokoban.Model.Player;

/**
 * Created by Рафек on 01.05.2017.
 */

public class PlayerTouchpad extends Touchpad {
    private Player player;
    private Camera camera;

    public PlayerTouchpad(float deadzoneRadius, TouchpadStyle style, Player player) {
        super(deadzoneRadius, style);
        this.player = player;
        this.camera = null;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public void act(float delta) {
        super.act(delta);

        player.rotateBy(-getKnobPercentX());
        player.moveBy(3 * getKnobPercentY() * (float) Math.cos(Math.toRadians(90 + player.getRotation())),
                      3 * getKnobPercentY() * (float) Math.sin(Math.toRadians(90 + player.getRotation()))
        );
        if (camera != null) {
            camera.position.y = player.getY();
            camera.position.x = player.getX();

        }
    }
}
