package ru.androiddevschool.sokoban.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import java.util.Random;

import ru.androiddevschool.std.Controller.ScreenTraveler;
import ru.androiddevschool.std.Screens.StdScreen;
import ru.androiddevschool.std.Utils.StdAssets;
import ru.androiddevschool.std.Utils.Values;

import static ru.androiddevschool.std.Utils.Names.ScreenName.HELP;

/**
 * Created by 06k1402 on 04.04.2017.
 */
public class Menu extends StdScreen {
    Random r;
    Table table;
    public Menu(StdAssets assets, AssetManager manager) {
        super(assets, manager);
    }

    @Override
    protected void initBg(Stage stage) {
        Image image;
        image = new Image(assets.images.get("bg0"));
        image.setSize(Values.WORLD_WIDTH, Values.WORLD_HEIGHT);

        stage.addActor(image);
    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        r = new Random();
        table = new Table();
        table.setFillParent(true);
        table.setDebug(true);

        Image img;

        int imagesCount = 6;

        for (int i = 0; i < 3; i++) {
            img = new Image(assets.images.get(String.format("%d_shadow", r.nextInt(imagesCount) + 1)));
            table.add(img).size(200).pad(20);
        }
        table.row();

        Button button = new Button(assets.buttonStyles.get("play"));
        button.addListener(new ScreenTraveler(ScreenName.PLAY));
        table.add(button).size(200).pad(30);

        button = new Button(assets.buttonStyles.get("settings"));
        button.addListener(new ScreenTraveler(ScreenName.SETTINGS));
        table.add(button).size(200).pad(30);

        button = new Button(assets.buttonStyles.get("help"));
        button.addListener(new ScreenTraveler(HELP));
        table.add(button).size(200).pad(30).row();

        stage.addActor(table);

    }
}

