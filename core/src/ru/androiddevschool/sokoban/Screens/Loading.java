package ru.androiddevschool.sokoban.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.MusicLoader;
import com.badlogic.gdx.assets.loaders.SoundLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ProgressBar;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.std.Screens.StdScreen;
import ru.androiddevschool.std.Utils.Global;
import ru.androiddevschool.std.Utils.StdAssets;
import ru.androiddevschool.std.Utils.Utils;
import ru.androiddevschool.std.Utils.Values;

/**
 * Created by Гриша on 10.05.2017.
 */
public class Loading extends StdScreen {
    private ProgressBar bar;
    private InternalFileHandleResolver resolver;

    public Loading(StdAssets assets, AssetManager manager) {
        super(assets, manager);
        setLoaders();
        loadAssets();
    }

    public void render(float delta) {
        super.render(delta);
        if (manager.update()) {
            //for (String name : manager.getAssetNames()) System.out.println(name);
            Global.game.postLoad();
        }
        bar.setValue(manager.getProgress());
    }

    private void loadAssets() {
        loadAssets("images/", "png", Texture.class);
        loadAssets("images/", "jpg", Texture.class);
        loadAssets("audio/sounds/", "ogg", Sound.class);
        loadAssets("audio/music/", "mp3", Music.class);
    }

    private void loadAssets(String path, String extension, Class type) {
        FileHandle folder = Gdx.files.internal(path);
        if (folder.isDirectory())
            for (FileHandle file : folder.list())
                loadAssets(file.path(), extension, type);
        if (folder.extension().equals(extension)) {
            manager.load(folder.path(), type);
        }
    }

    private void setLoaders() {
        resolver = new InternalFileHandleResolver();
        manager.setLoader(Music.class, new MusicLoader(resolver));
        manager.setLoader(Sound.class, new SoundLoader(resolver));
        manager.setLoader(Texture.class, new TextureLoader(resolver));
    }

    @Override
    protected void initBg(Stage stage) {
        Image bg = new Image(new Texture(Gdx.files.internal("loading/bg.png")));
        bg.setSize(Values.WORLD_WIDTH,Values.WORLD_HEIGHT);
        stage.addActor(bg);
    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        Skin progressbar = new Skin(Utils.handle("loading/loading.json"), new TextureAtlas(Utils.handle("loading/loading.atlas")));

        Table layout = new Table();
        layout.setFillParent(true);
        bar = new ProgressBar(0f, 1f, 0.01f, false, progressbar);
        layout.add(bar).width(Values.WORLD_WIDTH).expand().bottom();
        stage.addActor(layout);
    }
}
