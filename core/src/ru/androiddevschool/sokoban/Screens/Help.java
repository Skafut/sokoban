package ru.androiddevschool.sokoban.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.std.Controller.ScreenTraveler;
import ru.androiddevschool.std.Screens.StdScreen;
import ru.androiddevschool.std.Utils.StdAssets;

/**
 * Created by 06k1402 on 04.04.2017.
 */
public class Help extends StdScreen {
    public Help(StdAssets assets, AssetManager manager) {
        super(assets, manager);
    }
    @Override
    protected void initBg(Stage stage) {

    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        Table table = new Table();
        table.setFillParent(true);
        Button button = new Button(assets.buttonStyles.get("back"));
        button.addListener(new ScreenTraveler(ScreenName.MENU));
        table.add().expandX();
        table.add(button).row();
        table.add().colspan(2).expand();
        stage.addActor(table);

    }
}
