package ru.androiddevschool.sokoban.Screens;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.std.Controller.ScreenTraveler;
import ru.androiddevschool.std.Screens.StdScreen;
import ru.androiddevschool.std.Utils.StdAssets;


/**
 * Created by 06k1402 on 11.04.2017.
 */
public class Settings extends StdScreen {
    public Settings(StdAssets assets, AssetManager manager) {
        super(assets, manager);
    }

    @Override
    protected void initBg(Stage stage) {

    }

    @Override
    protected void initWorld(Stage stage) {

    }

    @Override
    protected void initUi(Stage stage) {
        Button button;
        Table table;
        table = new Table();
        table.setFillParent(true);
        table.setDebug(true);

        button = new Button(assets.buttonStyles.get("back"));
        button.addListener(new ScreenTraveler(ScreenName.MENU));
        table.add();
        table.add(button).row();

        table.add().colspan(2).expand().row();

        button = new Button(assets.buttonStyles.get("music"));
        table.add(button).padBottom(30);

        button = new Button(assets.buttonStyles.get("sounds"));
        table.add(button).padBottom(30).row();

        stage.addActor(table);

    }
}
