package ru.androiddevschool.sokoban.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.HashSet;

/**
 * Created by Admin on 16.05.2017.
 */

public class World extends Stage {
    public HashSet<Mob> mobs;
    public HashSet<Food> foods;
    public HashSet<Actor> toDelete;

    public World(Viewport viewport, Batch batch){
        super(viewport, batch);
    }

    public void addMob(Mob mob){

    }
    public void addPlayer(Player player){

    }
    public void addFood(Food food){

    }
}
