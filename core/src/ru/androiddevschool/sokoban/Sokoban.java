package ru.androiddevschool.sokoban;

import ru.androiddevschool.sokoban.Screens.*;
import ru.androiddevschool.std.StdGame;

import static ru.androiddevschool.std.Utils.Names.ScreenName.*;

public class Sokoban extends StdGame {
    private static StdGame instance = new Sokoban();

    private Sokoban() {
    }

    public static StdGame get() {
        return instance;
    }

    @Override
    public void create() {
        super.create();
        screens.put(LOADING, new Loading(assets, manager));
        setScreen(LOADING);
    }

    @Override
    public void postLoad() {
        if (assets == null) System.out.println("NULLMAIN");
        screens.put(MENU, new Menu(assets, manager));
        screens.put(SETTINGS, new Settings(assets, manager));
        screens.put(PLAY, new Play(assets, manager));
        screens.put(HELP, new Help(assets, manager));
        setScreen(MENU);
    }
}




